import { Component, Input, OnInit } from '@angular/core';
import { FirmModel } from '../models/models';

@Component({
  selector: 'app-firm-viewer',
  templateUrl: './firm-viewer.component.html',
  styleUrls: ['./firm-viewer.component.scss'],
})
export class FirmViewerComponent implements OnInit {
  @Input() firm?: FirmModel;
  constructor() { }

  ngOnInit() { }

  readyImage(): boolean {
    if (this.firm == null) return false;
    if (this.firm.images.length > 0 && this.firm.images[0].base64 != '') return true;
    return false;
  }

  getFirmHours(): string {
    if (this.firm == null) return "";
    if (this.firm.storeInfo == null) return "";
    if (this.firm.storeInfo.workingHours.length < 1) return "";

    var h = this.firm.storeInfo.workingHours[0];
    return `${h.open} ${h.close}`;
  }

  getFirmRate(): string {
    if (this.firm == null) return "";
    if (this.firm.storeInfo == null) return "";

    return this.firm.storeInfo.rate;
  }

  getMinOrderPrice(): string {
    if (this.firm == null) return "";
    if (this.firm.storeInfo == null) return "";
    if (this.firm.storeInfo.minOrderPrice == null) return "";

    return this.firm.storeInfo.minOrderPrice;
  }

  getFirmStatus(): string {
    if (this.firm == null) return "";
    if (this.firm.storeInfo == null) return "";

    return (this.firm.storeInfo.status == "open") ? "İşletme açık" : "İşletme kapalı";
  }

  getFirmStatusClassName(): string {
    let className = "text-style-firm-description";

    if (this.firm == null) return className;
    if (this.firm.storeInfo == null) return className;

    return className + ((this.firm.storeInfo.status == "open") ? " status-open" : " status-closed");
  }
}
