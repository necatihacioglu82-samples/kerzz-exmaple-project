import { Component } from '@angular/core';
import { InfiniteScrollCustomEvent } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FirmModel } from '../models/models';
import { Position } from '@capacitor/geolocation';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public firms: FirmModel[] = [];

  private lat: number = 0;
  private long: number = 0;
  private skip: number = 0;
  private limit: number = 10;

  constructor(public httpClient: HttpClient) {
    this.getCurrentLocation();
  }

  async getFirmsData(onDone?: Function, onError?: Function) {
    const headers = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        "apiKey": "bW9jay04ODc3NTU2NjExMjEyNGZmZmZmZmJ2",
      }),
    };

    const postData = {
      "skip": this.skip,
      "limit": this.limit,
      "latitude": this.lat.toString(),
      "longitude": this.long.toString(),
    }

    this.httpClient.post("https://smarty.kerzz.com:4004/api/mock/getFeed", postData, headers)
      .subscribe({
        next: (v: any) => {
          this.firms = [...this.firms, ...v.response];
          if (onDone != null) onDone();
        },
        error: (e) => {
          console.error(e);
          if (onError != null) onError();
        },
        complete: () => {
          console.info('complete');
        },
      });
  }

  async getCurrentLocation() {
    if (navigator.geolocation) {
      const options: PositionOptions = { enableHighAccuracy: true };
      navigator.geolocation.getCurrentPosition(
        (position: Position) => {
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
          console.log("Latitude : " + this.lat + " Longitude: " + this.long);
          this.getFirmsData();
        },
        () => { },
        options,
      );
    } else {
      // TODO: geolocation desteklenmiyor
      console.log("Sorry, browser does not support geolocation!");
    }
  }

  onIonInfinite(event: InfiniteScrollCustomEvent) {
    this.skip += this.limit;

    this.getFirmsData(
      () => {
        event.target.complete();
      }, () => {
        // TODO: api'den veri çekilemedi. yeniden dene
      },
    );
  }
}
