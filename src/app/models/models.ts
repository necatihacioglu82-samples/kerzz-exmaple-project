export interface FirmModel {
  id: string;
  title: string;
  text: string;
  type: string;
  images: FirmImageModel[];
  location: any;
  isDinner: boolean;
  isDelivery: boolean;
  storeInfo: any;
  categoryId: string;
}

export interface FirmImageModel {
  itemType: string,
  itemId: string,
  imageSize: string,
  base64: string,
  storeId: string,
}